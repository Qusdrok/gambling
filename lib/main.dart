import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'source/source.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIOverlays([]);
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: AppColors.primaryLight,
    ),
  );

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  await Firebase.initializeApp();
  runApp(WidgetRestart(child: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    initPrecacheLocal();
  }

  initPrecacheLocal() {
    precacheImage(AssetImage(AppImages.logo), context);
    precacheImage(AssetImage(AppImages.icFood1), context);
    precacheImage(AssetImage(AppImages.icFood2), context);
    precacheImage(AssetImage(AppImages.icFood3), context);
    precacheImage(AssetImage(AppImages.icFood4), context);
    precacheImage(AssetImage(AppImages.icFood5), context);
    precacheImage(AssetImage(AppImages.icFood6), context);
    precacheImage(AssetImage(AppImages.icFood7), context);
    precacheImage(AssetImage(AppImages.icFood8), context);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: AppDefaults.APP_NAME,
      theme: normalTheme(context),
      locale: Locale('vi', 'VN'),
      home: SplashScreen(),
      onGenerateRoute: Routers.generateRoute,
    );
  }
}
