import 'package:flutter/material.dart';

import '../source.dart';

class AppColors {
  AppColors._();

  static final Color primary = HexColor.fromHex("#445E37");
  static final Color primaryDark = HexColor.fromHex("#5A7D48");
  static final Color primaryLight = HexColor.fromHex("#ADBFA3");

  static final Color grey = HexColor.fromHex("#A1A1A1");
  static final Color greyLight = HexColor.fromHex("#D5DFD0");
  static final Color greyLight2 = HexColor.fromHex("#F4F9F2");

  static final Color white = HexColor.fromHex("#EFEFEF");
  static final Color white2 = HexColor.fromHex("#E8E8E8");
  static final Color white3 = HexColor.fromHex("#DEDEDE");

  static final Color orangeLight = HexColor.fromHex("#FEA947");
  static final Color yellow = HexColor.fromHex("#FFE600");

  static final Color blue = HexColor.fromHex("#386aef");
  static final Color lightBlue = HexColor.fromHex("#8edeff");

  static final Color black = HexColor.fromHex("#212121");
  static final Color red = HexColor.fromHex("#e01300");
}
