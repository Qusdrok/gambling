import 'dart:io';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';

import '../source.dart';

class AppFirebase {
  AppFirebase._();

  static final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  static final FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
  static final FirebaseStorage firebaseStorage = FirebaseStorage.instance;

  static void sendOTPPhoneNumber({
    String phoneNumber,
    int timeout = 60,
    Function(PhoneAuthCredential credential) verificationCompleted,
    Function(FirebaseAuthException ex) verificationFailed,
    Function(String verificationId, int forceResendingToken) codeSent,
    Function(String verificationId) codeTimeout,
  }) async {
    await firebaseAuth.verifyPhoneNumber(
      phoneNumber: AppHelper.convertPhoneNumber(phoneNumber.trim()),
      timeout: Duration(seconds: timeout),
      verificationCompleted: verificationCompleted,
      verificationFailed: verificationFailed,
      codeSent: codeSent,
      codeAutoRetrievalTimeout: codeTimeout,
    );
  }

  static CollectionReference userCollection =
      firebaseFirestore.collection("users");

  static Future<void> uploadUser(UserModel userModel) async {
    await userCollection.doc("userID-${userModel.id}").set(userModel.toJson());
  }

  static Future<void> updateUserCoin({int coin, int profit = 0}) async {
    var doc = await userCollection.doc("userID-${Data.userModel.id}").get();
    UserModel userModel = UserModel.fromJson(doc.data());

    if (coin <= userModel.currentCoin) {
      userModel.addCoin(coin);
      userModel.addProfit(profit);

      Data.userModel = userModel;
      await userCollection
          .doc("userID-${Data.userModel.id}")
          .update(userModel.toJson());
    }
  }

  static Future<void> updateUserCoinType() async {
    var doc = await userCollection.doc("userID-${Data.userModel.id}").get();
    UserModel userModel = UserModel.fromJson(doc.data());
    userModel.setCoinType();

    Data.userModel = userModel;
    await userCollection
        .doc("userID-${Data.userModel.id}")
        .update(userModel.toJson());
  }

  static Future<void> updateSlotWin({
    String name = "",
    int coin = 0,
    int profit = 0,
  }) async {
    var data = await firebaseFirestore.collection("slots").get();
    var docs = data.docs;

    for (int i = 0; i < docs.length; i++) {
      var item = docs[i].data();
      if (Data.slotModels.length < 8) {
        Data.slotModels.add(SlotModel.fromJson(item));
      }

      if (item["name"] == name) {
        if (coin > 0) {
          await updateUserCoin(
            coin: coin + coin * Data.slotModels[i].win,
            profit: profit,
          );
        }

        Data.slotModels[i].win = item["win"]++;
        await firebaseFirestore
            .collection("slots")
            .doc("slots-${i}")
            .update(Data.slotModels[i].toJson());
      }
    }
  }

  static Future<UploadTask> uploadImageFile(File file) async {
    if (file == null) return null;
    Reference ref = firebaseStorage.ref().child('media').child(
          '/${DateTime.now().millisecondsSinceEpoch}.jpg',
        );

    final metadata = SettableMetadata(
      contentType: 'image/jpeg',
      customMetadata: {
        'picked-file-path': file.path,
      },
    );

    UploadTask uploadTask = ref.putFile(File(file.path), metadata);
    return Future.value(uploadTask);
  }

  static Future<Uint8List> downloadBytes(Reference ref) async =>
      await ref.getData();

  static Future<String> downloadLink(Reference ref) async =>
      await ref.getDownloadURL();
}
