import 'package:flutter/material.dart';

class AppImages {
  AppImages._();

  // Icon Data
  static const IconData icDataTick =
      IconData(0xe156, fontFamily: 'MaterialIcons');

  // Logo
  static final String logo = 'assets/logo/logo.png';

  // Background
  static final String imgBg = "assets/bgs/bg.png";

  // Buttons
  static final String btnFB = "assets/btns/fb.png";
  static final String btnGoogle = "assets/btns/google.png";

  // Icon
  static final String icAvatar = "assets/icons/avatar.png";
  static final String icBigoResult = "assets/icons/bigo_result.png";

  static final String icChooseCoinRed = "assets/icons/choose-coin-red.png";
  static final String icChooseCoinWhite = "assets/icons/choose-coin-white.png";

  static final String icCoin1 = "assets/icons/coin1.png";
  static final String icCoin2 = "assets/icons/coin2.png";
  static final String icCoin3 = "assets/icons/coin2.png";

  static final String icCoin10 = "assets/icons/coin-10.png";
  static final String icCoin50 = "assets/icons/coin-50.png";
  static final String icCoin100 = "assets/icons/coin-100.png";
  static final String icCoin1000 = "assets/icons/coin-1000.png";

  static final String icFood1 = "assets/icons/food1.png";
  static final String icFood2 = "assets/icons/food2.png";
  static final String icFood3 = "assets/icons/food3.png";
  static final String icFood4 = "assets/icons/food4.png";
  static final String icFood5 = "assets/icons/food5.png";
  static final String icFood6 = "assets/icons/food6.png";
  static final String icFood7 = "assets/icons/food7.png";
  static final String icFood8 = "assets/icons/food8.png";

  static final String icDiamond = "assets/icons/kim-cuong-2.png";

  static final String icPizza = "assets/icons/pizza.png";
  static final String icPizzaBuzz = "assets/icons/pizza-buzz.png";

  static final String icSalad = "assets/icons/salad.png";
  static final String icSaladBuzz = "assets/icons/salad-buzz.png";

  static final String icLayer1 = "assets/icons/layer-2.png";
  static final String icLayer2 = "assets/icons/layer-4.png";
  static final String icLayer3 = "assets/icons/layer-5.png";

  static final String icFreeCoin = "assets/icons/free_coin.png";
  static final String icPaidCoin = "assets/icons/paid_coin.png";
}
