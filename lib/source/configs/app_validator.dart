import 'package:flutter/material.dart';

enum ValidatePasswordType {
  LN,
  LNSC,
  ULLN,
  ULLNSC8,
  ULLNSC10,
}

class AppValidator {
  AppValidator._();

  static Pattern patternEmail =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  static Pattern patternPhone = r'^(?:[+0]9)?[0-9]{10}$';

  // Minimum 8 characters, at least one letter and one number (LN)
  static Pattern patternPassword1 = r"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$";

  // Minimum 8 characters, at least one letter, one number and one special character (LNSC)
  static Pattern patternPassword2 =
      r"^(?=.*[A-Za-z])(?=.*d)(?=.*[@$!%*#?&])[A-Za-zd@$!%*#?&]{8,}$";

  // Minimum 8 characters, at least one uppercase letter, one lowercase letter and one number (ULLN)
  static Pattern patternPassword3 =
      r"^(?=.*[a-z])(?=.*[A-Z])(?=.*d)[a-zA-Zd]{8,}$";

  // Minimum 8 characters, at least one uppercase letter, one lowercase letter, one number and one special character (ULLNSC8)
  static Pattern patternPassword4 =
      r"^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[@$!%*?&])[A-Za-zd@$!%*?&]{8,}$";

  // Minimum eight and maximum 10 characters, at least one uppercase letter, one lowercase letter, one number and one special character (ULLNSC10)
  static Pattern patternPassword5 =
      r"^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[@$!%*?&])[A-Za-zd@$!%*?&]{8,10}$";

  static RegExp regexPassword = new RegExp(patternPassword1);
  static RegExp regexEmail = new RegExp(patternEmail);
  static RegExp regexPhone = new RegExp(patternPhone);

  static validateFullName(BuildContext context) {
    return (value) {
      if (value == null || value.length == 0) return "Tên không được để trống";
      return null;
    };
  }

  static validateNumber(BuildContext context) {
    return (value) {
      if (value == null || value.length == 0) {
        return "Trường này không được để trống";
      } else if (!(value is int)) {
        return "Trường này chỉ có thể là số";
      }

      return null;
    };
  }

  static validateEmail(BuildContext context) {
    return (value) {
      if (value == null || value.length == 0) {
        return "Email không được để trống";
      } else {
        if (!regexEmail.hasMatch(value)) return "Email không đúng định dạng";
        return null;
      }
    };
  }

  static validateEmailOrPhoneNumber(BuildContext context) {
    return (value) {
      if (value == null || value.length == 0) {
        return "Email hoặc số điện thoại không được để trống";
      } else {
        if (!regexEmail.hasMatch(value)) {
          return "Email không đúng đinh dạng";
        } else {
          if (value.length > 10) {
            return "Số điện thoại quá dài";
          } else if (value.length < 10) {
            return "Số điện thoại quá ngắn";
          } else if (!regexPhone.hasMatch(value)) {
            return "Số điện thoại không đúng định dạng";
          }
        }

        return null;
      }
    };
  }

  static validatePassword(
    BuildContext context, {
    int length = 8,
    ValidatePasswordType validatePasswordType = ValidatePasswordType.LN,
  }) {
    return (value) {
      if (value == null || value.length == 0) {
        return "Mật khẩu không được để trống";
      } else if (value.length < length) {
        return "Mật khẩu phải chứa ít nhất 8 ký tự";
      } else {
        switch (validatePasswordType) {
          case ValidatePasswordType.LN:
            if (new RegExp(patternPassword1).hasMatch(value)) return null;
            return "Mật khẩu phải có 1 chữ và số";
          case ValidatePasswordType.LNSC:
            if (new RegExp(patternPassword2).hasMatch(value)) return null;
            return "Mật khẩu phải có 1 chữ, số và ký tự đặc biệt";
          case ValidatePasswordType.ULLN:
            if (new RegExp(patternPassword3).hasMatch(value)) return null;
            return "Mật khẩu phải có 1 chữ thường, hoa, số";
          case ValidatePasswordType.ULLNSC8:
            if (new RegExp(patternPassword4).hasMatch(value)) return null;
            return "Mật khẩu phải có 1 chữ thường, hoa, số và ký tự đặc biệt";
          case ValidatePasswordType.ULLNSC10:
            if (new RegExp(patternPassword5).hasMatch(value)) return null;
            return "Mật khẩu phải có 1 chữ thường, hoa, số và ký tự đặc biệt";
        }
      }
    };
  }

  static validateConfirmPassword(
    BuildContext context, {
    @required TextEditingController password,
    int length = 8,
    ValidatePasswordType validatePasswordType = ValidatePasswordType.LN,
  }) {
    return (value) {
      if (value == null || value.length == 0) {
        return "Mật khẩu không được để trống";
      } else if (value < length) {
        return "Mật khẩu phải chứa ít nhất 8 ký tự";
      } else {
        switch (validatePasswordType) {
          case ValidatePasswordType.LN:
            if (new RegExp(patternPassword1).hasMatch(value)) {
              if (password.text.compareTo(value) == 0) return null;
              return "Mật khẩu nhập lại không giống nhau";
            }

            return "Mật khẩu phải có 1 chữ và số";
          case ValidatePasswordType.LNSC:
            if (new RegExp(patternPassword2).hasMatch(value)) {
              if (password.text.compareTo(value) == 0) return null;
              return "Mật khẩu nhập lại không giống nhau";
            }

            return "Mật khẩu phải có 1 chữ, số và ký tự đặc biệt";
          case ValidatePasswordType.ULLN:
            if (new RegExp(patternPassword3).hasMatch(value)) {
              if (password.text.compareTo(value) == 0) return null;
              return "Mật khẩu nhập lại không giống nhau";
            }

            return "Mật khẩu phải có 1 chữ thường, hoa, số";
          case ValidatePasswordType.ULLNSC8:
            if (new RegExp(patternPassword4).hasMatch(value)) {
              if (password.text.compareTo(value) == 0) return null;
              return "Mật khẩu nhập lại không giống nhau";
            }

            return "Mật khẩu phải có ít nhất 8 từ và 1 chữ thường, hoa, số và ký tự đặc biệt";
          case ValidatePasswordType.ULLNSC10:
            if (new RegExp(patternPassword5).hasMatch(value)) {
              if (password.text.compareTo(value) == 0) return null;
              return "Mật khẩu nhập lại không giống nhau";
            }

            return "Mật khẩu phải có ít nhất 10 từ và 1 chữ thường, hoa, số và ký tự đặc biệt";
        }
      }
    };
  }

  static validatePhoneNumber(BuildContext context) {
    return (value) {
      if (value == null || value.length == 0) {
        return "Số điện thoại không để trống";
      } else if (value.length > 10) {
        return "Số điện thoại quá dài";
      } else if (value.length < 10) {
        return "Số điện thoại quá ngắn";
      } else if (!regexPhone.hasMatch(value)) {
        return "Số điện thoại không đúng định dạng";
      }

      return null;
    };
  }
}
