class SlotModel {
  String img;
  String name;

  int win;
  int showCoin;

  bool isSelected;

  SlotModel({
    this.img,
    this.name,
    this.win = 0,
    this.showCoin = 0,
    this.isSelected = false,
  });

  void addCoin() {
    this.showCoin++;
  }

  void addWin() {
    this.win++;
  }

  void setDisable() {
    isSelected = false;
    showCoin = 0;
  }

  factory SlotModel.fromJson(Map<String, dynamic> json) {
    return SlotModel(
      name: json['name'],
      win: json['win'],
      img: json['img'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "img": img,
      "name": name,
      "win": win,
    };
  }
}
