class UserModel {
  List<UserModel> user_friends = [];
  String user_avatar, created_at, updated_at, id, name;
  int free_profit, paid_profit, free_coin, paid_coin, ranking, type_coin;
  bool isSelected, isRegister;

  UserModel({
    this.name,
    this.user_avatar,
    this.created_at,
    this.updated_at,
    this.id,
    this.free_profit = 0,
    this.paid_profit = 0,
    this.free_coin = 10000,
    this.paid_coin = 0,
    this.type_coin = 0,
    this.ranking = 0,
    this.isSelected = false,
    this.isRegister = true,
    this.user_friends = const [],
  });

  int get currentCoin {
    return type_coin == 0 ? free_coin : paid_coin;
  }

  int get currentProfit {
    return type_coin == 0 ? free_profit : paid_profit;
  }

  void setCoinType() {
    type_coin = type_coin == 0 ? 1 : 0;
  }

  void addCoin(int coin) {
    if (type_coin == 0) {
      this.free_coin += coin;
    } else {
      this.paid_coin += coin;
    }
  }

  void addProfit(int profit) {
    if (type_coin == 0) {
      this.free_profit += profit;
    } else {
      this.paid_profit += profit;
    }
  }

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json['id'],
      user_avatar: json['user_avatar'],
      created_at: json['created_at'],
      updated_at: json['updated_at'],
      free_profit: json['free_profit'],
      paid_profit: json['paid_profit'],
      free_coin: json['free_coin'],
      paid_coin: json['paid_coin'],
      type_coin: json['type_coin'],
      ranking: json['ranking'],
      name: json['name'],
      isRegister: json['isRegister'],
      user_friends: List<UserModel>.from(
        json["user_friends"].map((x) => UserModel.fromJson(x)),
      ),
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'user_avatar': user_avatar,
        'created_at': created_at,
        'updated_at': updated_at,
        'free_profit': free_profit,
        'paid_profit': paid_profit,
        'free_coin': free_coin,
        'paid_coin': paid_coin,
        'type_coin': type_coin,
        'ranking': ranking,
        'name': name,
        'isRegister': isRegister,
        'user_friends': List<dynamic>.from(
          user_friends.map((x) => x.toJson()),
        ),
      };
}
