import 'package:flutter/material.dart';

import '../../source.dart';

class ExchangeScreen extends StatefulWidget {
  @override
  _ExchangeScreenState createState() => _ExchangeScreenState();
}

class _ExchangeScreenState extends State<ExchangeScreen> {
  ExchangeViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      viewModel: ExchangeViewModel(),
      onViewModelReady: (vm) => _viewModel = vm..init(),
      builder: (context, vm, child) {
        return Scaffold(
          body: _buildBody(),
        );
      },
    );
  }

  Widget _buildBody() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: WidgetTextFormField(
            hintText: "Tìm kiếm",
            controller: _viewModel.searchController,
            onSubmitted: (keyword) => _viewModel.searchUser(keyword),
            width: 20,
            height: 10,
          ),
        ),
        Container(
          width: AppSize.screenWidth,
          height: 1,
          color: Colors.black26,
          margin: EdgeInsets.symmetric(vertical: 10),
        ),
        Expanded(
          child: StreamBuilder(
            stream: _viewModel.isLoadingSubject,
            builder: (context, snapshot) {
              var data = _viewModel.searchResult;
              bool enabled = data == null;

              return enabled
                  ? WidgetShimmer(
                      child: ListView.separated(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        separatorBuilder: (context, index) => Container(
                          width: AppSize.screenWidth,
                          height: 1,
                          color: Colors.black26,
                          margin: EdgeInsets.symmetric(vertical: 10),
                          padding: EdgeInsets.symmetric(horizontal: 15),
                        ),
                        itemBuilder: (context, index) =>
                            _buildSearchResult(null),
                        itemCount: 2,
                      ),
                    )
                  : data.length > 0
                      ? ListView.separated(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          separatorBuilder: (context, index) => Container(
                            width: AppSize.screenWidth,
                            height: 1,
                            color: Colors.black26,
                            margin: EdgeInsets.symmetric(vertical: 10),
                            padding: EdgeInsets.symmetric(horizontal: 15),
                          ),
                          itemCount: data.length,
                          physics: BouncingScrollPhysics(),
                          itemBuilder: (context, index) =>
                              _buildSearchResult(data[index]),
                        )
                      : Center(
                          child: Text(
                            "Không có kết quả",
                            style: AppStyles.DEFAULT_LARGE,
                            textAlign: TextAlign.center,
                          ),
                        );
            },
          ),
        ),
        StreamBuilder(
          stream: _viewModel.isLoadingSubject,
          builder: (context, snapshot) {
            bool isLoading = snapshot.data ?? false;
            return Padding(
              padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
              child: Row(
                children: [
                  Expanded(
                    child: WidgetTextFormField(
                      hintText: "Nhập điểm cần đổi",
                      controller: _viewModel.pointController,
                      inputType: TextInputType.number,
                      height: 10,
                      readOnly: isLoading,
                    ),
                  ),
                  SizedBox(width: 20),
                  GestureDetector(
                    onTap: () => _viewModel.exchangeMoney(),
                    child: Container(
                      width: 110,
                      height: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.redAccent,
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      alignment: Alignment.center,
                      child: !isLoading
                          ? Text(
                              "Chuyển tiền",
                              style: AppStyles.DEFAULT_REGULAR.copyWith(
                                color: Colors.white,
                              ),
                            )
                          : WidgetLoading(
                              dotOneColor: Colors.white,
                              dotTwoColor: Colors.white,
                              dotThreeColor: Colors.white,
                            ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ],
    );
  }

  Widget _buildSearchResult(UserModel userModel) {
    return GestureDetector(
      onTap: () => _viewModel.setUserSelected(userModel),
      child: Row(
        children: [
          CircleAvatar(
            radius: 25,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(99),
              child: userModel == null
                  ? Data.userAvatar
                  : WidgetImageNetwork(
                      url: userModel.user_avatar,
                      fit: BoxFit.fill,
                    ),
            ),
          ),
          SizedBox(width: 15),
          Text(
            userModel?.name ?? "User",
            style: AppStyles.DEFAULT_REGULAR,
          ),
          Spacer(),
          Container(
            width: 25,
            height: 25,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: (userModel?.isSelected ?? true)
                  ? Colors.redAccent
                  : Colors.transparent,
              border: Border.all(
                color: (userModel?.isSelected ?? true)
                    ? Colors.transparent
                    : Colors.grey,
                width: 2,
              ),
            ),
            child: Icon(
              AppImages.icDataTick,
              color:
                  (userModel?.isSelected ?? true) ? Colors.white : Colors.grey,
              size: 20,
            ),
          ),
        ],
      ),
    );
  }
}
