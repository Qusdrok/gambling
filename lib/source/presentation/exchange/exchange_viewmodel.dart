import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../../source.dart';

class ExchangeViewModel extends BaseViewModel {
  TextEditingController searchController = TextEditingController();
  TextEditingController pointController = TextEditingController();

  List<UserModel> searchResult = [];
  List<UserModel> userSelected = [];
  List<UserModel> userFriends = [];
  bool isDontFindUser = false;

  init() {
    searchUser("");
  }

  exchangeMoney() async {
    if (pointController.text.isEmpty) {
      Toast.show("Bạn chưa nhập tiền cần chuyển", context);
    } else if (Data.userModel.free_coin < int.parse(pointController.text)) {
      Toast.show("Bạn không có đủ tiền", context);
    } else if (int.parse(pointController.text) > 9223372036854775807) {
      Toast.show("Bạn đã nhập số tiền quá lớn", context);
    } else if (userSelected.isEmpty) {
      Toast.show("Bạn chưa chọn người cần chuyển tiền", context);
    } else {
      setLoading(true);
      var money = int.parse(pointController.text);
      var doc = await AppFirebase.userCollection
          .doc("userID-${Data.userModel.id}")
          .get();
      UserModel userModel = UserModel.fromJson(doc.data());

      for (var x in userSelected) {
        if (userModel.free_coin >= money) {
          var user =
              await AppFirebase.userCollection.doc("userID-${x.id}").get();
          var um = UserModel.fromJson(user.data());

          um.addCoin(money);
          userModel.addCoin(-money);

          await AppFirebase.userCollection
              .doc("userID-${x.id}")
              .update(um.toJson());

          if (!Data.userModel.user_friends.contains(um)) {
            Data.userModel.user_friends.add(um);
          }

          await AppFirebase.userCollection
              .doc("userID-${Data.userModel.id}")
              .update(userModel.toJson());

          Toast.show(
            "Bạn đã chuyển thành công cho ${x.name}",
            context,
            duration: 2,
          );
        } else {
          Toast.show(
            "Bạn đã hết tiền để chuyển cho ${x.name}",
            context,
            duration: 2,
          );

          break;
        }
      }

      setLoading(false);
    }
  }

  searchUser(String keyword) async {
    setLoading(true);
    searchResult = null;

    if (keyword.isEmpty) {
      searchResult = [];
      for (int i = 0; i < Data.userModel.user_friends.length; i++) {
        searchResult.add(Data.userModel.user_friends[i]);
      }
    } else {
      searchResult = [];
      var data = await AppFirebase.firebaseFirestore.collection("users").get();
      var docs = data.docs;

      for (int i = 0; i < docs.length; i++) {
        var item = docs[i].data();
        if (item["name"]
                .toString()
                .toLowerCase()
                .contains(keyword.toLowerCase()) &&
            item["id"] != Data.userModel.id) {
          searchResult.add(UserModel.fromJson(item));
        }
      }
    }

    setLoading(false);
  }

  setUserSelected(UserModel userModel) {
    if (userModel == null) return;
    if (userSelected.contains(userModel)) return;

    var index = searchResult.indexOf(userModel);
    searchResult[index].isSelected = true;

    userSelected.add(userModel);
    notifyListeners();
  }
}
