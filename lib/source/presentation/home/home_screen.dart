import 'dart:async';

import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';

import '../../source.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  AnimationController _selectTimeAC;
  AnimationController _resultTimeAC;
  HomeViewModel _viewModel;

  String timerString(AnimationController ac) {
    var duration = ac.duration * ac.value;
    var seconds = _viewModel.isResultTime ? 5 : 30;
    return '${seconds - (duration.inSeconds % 60)}s';
  }

  @override
  void initState() {
    super.initState();
    _selectTimeAC = AnimationController(
      vsync: this,
      duration: Duration(seconds: 30),
    );

    _resultTimeAC = AnimationController(
      vsync: this,
      duration: Duration(seconds: 5),
    );

    _resultTimeAC.addStatusListener(
      (status) async {
        switch (status) {
          case AnimationStatus.completed:
            var rs = await _viewModel.destroySS();
            if (rs ?? true) {
              _resultTimeAC.stop();
              _selectTimeAC.reset();
              _selectTimeAC.forward();
              _viewModel.setIsResultTime();
            }

            break;

          case AnimationStatus.forward:
            _viewModel.initSS();
            break;

          case AnimationStatus.dismissed:
            break;

          case AnimationStatus.reverse:
            break;
        }
      },
    );

    _selectTimeAC.addStatusListener(
      (status) {
        if (status == AnimationStatus.completed) {
          _selectTimeAC.stop();
          _resultTimeAC.reset();
          _resultTimeAC.forward();
          _viewModel.setIsResultTime();
        }
      },
    );

    Timer.run(() => _selectTimeAC.forward());
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      viewModel: HomeViewModel(),
      onViewModelReady: (vm) => _viewModel = vm..init(),
      builder: (context, vm, child) {
        return WillPopScope(
          onWillPop: () async => false,
          child: Scaffold(
            key: _viewModel.scaffoldKey,
            backgroundColor: AppColors.primaryLight,
            body: Stack(
              children: [
                Image.asset(
                  AppImages.imgBg,
                  height: AppSize.screenHeight,
                  width: AppSize.screenWidth,
                  fit: BoxFit.fill,
                ),
                Column(
                  children: [
                    _buildAppBar(),
                    Expanded(child: _buildBody()),
                  ],
                ),
                StreamBuilder(
                  stream: _viewModel.isLoadingSubject,
                  builder: (context, snapshot) {
                    var enabled = snapshot.data ?? false;
                    return enabled
                        ? Container(
                            width: AppSize.screenWidth,
                            height: AppSize.screenHeight,
                            color: Colors.black54,
                            alignment: Alignment.center,
                            child: Text(
                              "Loading ...",
                              style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
                                color: Colors.white,
                              ),
                            ),
                          )
                        : Container();
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildAppBar() {
    return Container(
      height: AppValues.HEIGHT_APP_BAR,
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Today's ${_viewModel.round} round",
            style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
              color: AppColors.red,
            ),
          ),
          GestureDetector(
            onTap: () async {
              if (_resultTimeAC.isAnimating) return;
              _selectTimeAC.stop();

              var rs = await Navigator.pushNamed(context, Routers.navigation);
              if (rs is bool && rs) {
                _selectTimeAC.reset();
                _selectTimeAC.forward();
              }
            },
            child: Container(
              height: 30,
              width: 75,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
                border: Border.all(
                  color: AppColors.black,
                  width: 1.5,
                ),
              ),
              padding: EdgeInsets.only(left: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Shop  ",
                    style: AppStyles.DEFAULT_REGULAR,
                  ),
                  Icon(
                    Icons.arrow_forward_ios_rounded,
                    size: 13,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    var controller = _viewModel.isResultTime ? _resultTimeAC : _selectTimeAC;
    var isDiamond = Data.userModel.type_coin == 1;

    return Stack(
      children: [
        Column(
          children: [
            Expanded(
              child: Stack(
                alignment: Alignment.center,
                clipBehavior: Clip.none,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 50),
                    child: Image.asset(
                      AppImages.icLayer3,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    top: -15,
                    child: _buildFoodSlot(index: 0),
                  ),
                  Positioned(
                    top: 15,
                    right: AppSize.screenWidth / 2.8,
                    child: _buildFoodSlot(index: 1),
                  ),
                  Positioned(
                    top: 15,
                    left: AppSize.screenWidth / 2.8,
                    child: _buildFoodSlot(index: 7),
                  ),
                  Positioned(
                    top: 95,
                    right: AppSize.screenWidth / 2.3,
                    child: _buildFoodSlot(index: 2),
                  ),
                  Positioned(
                    top: 95,
                    left: AppSize.screenWidth / 2.3,
                    child: _buildFoodSlot(index: 6),
                  ),
                  Positioned(
                    top: 175,
                    right: AppSize.screenWidth / 2.8,
                    child: _buildFoodSlot(index: 3),
                  ),
                  Positioned(
                    top: 175,
                    left: AppSize.screenWidth / 2.8,
                    child: _buildFoodSlot(index: 5),
                  ),
                  Positioned(
                    top: 210,
                    child: _buildFoodSlot(index: 4),
                  ),
                  Positioned(
                    top: 91,
                    child: Container(
                      height: 80,
                      width: 80,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: Column(
                        children: [
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.yellowAccent,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(99),
                                  topRight: Radius.circular(99),
                                ),
                              ),
                              alignment: Alignment.center,
                              child: Image.asset(
                                AppImages.logo,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              width: 80,
                              decoration: BoxDecoration(
                                color: AppColors.red,
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(99),
                                  bottomRight: Radius.circular(99),
                                ),
                              ),
                              child: AnimatedBuilder(
                                animation: controller,
                                builder: (context, child) => Column(
                                  children: [
                                    Text(
                                      "${_viewModel.isResultTime ? "Result" : "Select"} time",
                                      style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                        color: Colors.white,
                                      ),
                                    ),
                                    Text(
                                      timerString(controller),
                                      style: AppStyles.DEFAULT_REGULAR_BOLD
                                          .copyWith(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: Stack(
                clipBehavior: Clip.none,
                alignment: Alignment.topCenter,
                children: [
                  Positioned.fill(
                    top: 75,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(AppImages.icLayer1),
                          fit: BoxFit.fill,
                        ),
                      ),
                      padding: EdgeInsets.only(top: 35, left: 15, right: 15),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Row(
                              children: [
                                GestureDetector(
                                  onTap: () => _viewModel.updateCoinType(),
                                  child: Image.asset(
                                    isDiamond
                                        ? AppImages.icPaidCoin
                                        : AppImages.icFreeCoin,
                                    width: 45,
                                    height: 45,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                SizedBox(width: 10),
                                Expanded(
                                  child: _buildCoinInformation(
                                    title: "Coins left",
                                    coin: Data.userModel.currentCoin,
                                    isDiamond: isDiamond,
                                  ),
                                ),
                                SizedBox(width: 10),
                                Expanded(
                                  child: _buildCoinInformation(
                                    title: "Today's profits",
                                    coin: Data.userModel.currentProfit,
                                    isDiamond: true,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 5),
                            _buildResult(),
                            Container(
                              width: AppSize.screenWidth,
                              padding: EdgeInsets.symmetric(vertical: 4),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "My Record ",
                                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                      color: Colors.white,
                                    ),
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios_rounded,
                                    size: 10,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            ),
                            _buildRankInformation(),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0,
                    left: 15,
                    right: 15,
                    child: Container(
                      width: AppSize.screenWidth,
                      height: 100,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(AppImages.icLayer2),
                          fit: BoxFit.fill,
                        ),
                      ),
                      padding: EdgeInsets.only(bottom: 7.5),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            "Choose the amount of wager > choose food",
                            style: AppStyles.DEFAULT_MEDIUM,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 5),
                          Wrap(
                            spacing: 15,
                            children: List.generate(
                              4,
                              (index) => GestureDetector(
                                onTap: () => _viewModel.setCoinSelected(index),
                                child: Container(
                                  width: 50,
                                  height: 50,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage(
                                        _viewModel.coinSelected == index
                                            ? AppImages.icChooseCoinRed
                                            : AppImages.icChooseCoinWhite,
                                      ),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.fromLTRB(2.5, 10, 2.5, 5),
                                  child: Image.asset(
                                    _viewModel.coins[index].img,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: -35,
                    child: Container(
                      width: AppSize.screenWidth,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Image.asset(
                            AppImages.icSalad,
                            width: 40,
                            height: 50,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(),
                          Image.asset(
                            AppImages.icPizza,
                            width: 40,
                            height: 50,
                            fit: BoxFit.fill,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildRankInformation() {
    return Container(
      width: AppSize.screenWidth,
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: Colors.black,
          width: 1.5,
        ),
        color: Colors.white,
      ),
      padding: EdgeInsets.all(5),
      child: Row(
        children: [
          Stack(
            clipBehavior: Clip.none,
            children: [
              Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: Colors.black,
                    width: 0.5,
                  ),
                ),
                alignment: Alignment.center,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(99),
                  child: Data.userAvatar,
                ),
              ),
              Positioned(
                bottom: 0,
                right: -2.5,
                child: Container(
                  width: 15,
                  height: 15,
                  decoration: BoxDecoration(
                    color: Colors.yellow,
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Colors.black,
                      width: 0.5,
                    ),
                  ),
                  alignment: Alignment.center,
                  child: Text(
                    "1",
                    style: AppStyles.DEFAULT_SMALL,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(width: 5),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Today's Profit Ranking",
                style: AppStyles.DEFAULT_MEDIUM,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    AppImages.icDiamond,
                    width: 12,
                    height: 12,
                    fit: BoxFit.fill,
                  ),
                  Text(
                    " ${Data.userModel.currentProfit}",
                    style: AppStyles.DEFAULT_MEDIUM,
                  ),
                ],
              ),
            ],
          ),
          Spacer(),
          Icon(
            Icons.arrow_forward_ios_rounded,
            size: 15,
          ),
        ],
      ),
    );
  }

  Widget _buildResult() {
    return Container(
      height: 50,
      width: AppSize.screenWidth,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.redAccent,
      ),
      padding: EdgeInsets.only(bottom: 4),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              "Result",
              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                color: Colors.white,
              ),
            ),
          ),
          SizedBox(width: 10),
          Container(
            width: 0.5,
            height: 30,
            color: Colors.black,
          ),
          Expanded(
            child: _viewModel.resultFoods.length <= 0
                ? Center(
                    child: Text(
                      "Không có dữ liệu",
                      style: AppStyles.DEFAULT_MEDIUM.copyWith(
                        color: Colors.white,
                      ),
                    ),
                  )
                : ListView.separated(
                    scrollDirection: Axis.horizontal,
                    padding: EdgeInsets.fromLTRB(10, 5, 10, 8),
                    itemBuilder: (context, index) => Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Container(
                          width: 35,
                          height: 35,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(
                              width: 1.5,
                              color: Colors.black,
                            ),
                          ),
                          alignment: Alignment.center,
                          child: Image.asset(
                            _viewModel.getFoodImage(index),
                            fit: BoxFit.fill,
                          ),
                        ),
                        if (index == 0)
                          Positioned(
                            bottom: -7.5,
                            child: Container(
                              height: 15,
                              width: 35,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.yellow,
                                border: Border.all(
                                  width: 1.5,
                                  color: Colors.black,
                                ),
                              ),
                              alignment: Alignment.center,
                              child: Text(
                                "New",
                                style: AppStyles.DEFAULT_SMALL,
                              ),
                            ),
                          ),
                      ],
                    ),
                    separatorBuilder: (context, index) => Container(width: 5),
                    itemCount: _viewModel.resultFoods.length > 7
                        ? 7
                        : _viewModel.resultFoods.length,
                  ),
          ),
        ],
      ),
    );
  }

  Widget _buildCoinInformation({
    String title,
    int coin,
    bool isDiamond = false,
  }) {
    return Container(
      height: 45,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30),
        border: Border.all(
          color: Colors.black,
          width: 1,
        ),
      ),
      padding: EdgeInsets.symmetric(horizontal: 2),
      child: Row(
        children: [
          CircleAvatar(
            radius: 2,
            backgroundColor: Colors.black,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title,
                  style: AppStyles.DEFAULT_MEDIUM,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      isDiamond ? AppImages.icDiamond : AppImages.icCoin1,
                      width: 15,
                      height: 15,
                      fit: BoxFit.fill,
                    ),
                    Text(
                      " $coin",
                      style: AppStyles.DEFAULT_MEDIUM,
                    ),
                  ],
                ),
              ],
            ),
          ),
          CircleAvatar(
            radius: 2,
            backgroundColor: Colors.black,
          ),
        ],
      ),
    );
  }

  Widget _buildFoodSlot({int index}) {
    SlotModel slot = Data.slotModels[index];
    return GestureDetector(
      onTap: _viewModel.isResultTime
          ? () {}
          : () => _viewModel.setFoodSelected(index),
      child: Container(
        height: 75,
        width: 75,
        decoration: BoxDecoration(
          color: slot.isSelected ? AppColors.red : AppColors.blue,
          shape: BoxShape.circle,
          border: Border.all(
            color: Colors.black,
            width: 1,
          ),
        ),
        alignment: Alignment.center,
        child: Container(
          height: 68,
          width: 68,
          decoration: BoxDecoration(
            color: Colors.transparent,
            shape: BoxShape.circle,
            border: Border.all(
              color: Colors.black,
              width: 1,
            ),
          ),
          child: Column(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50),
                    ),
                  ),
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      Expanded(
                        child: Image.asset(
                          slot.img,
                          fit: BoxFit.fill,
                        ),
                      ),
                      if (_viewModel.bets[slot] != null)
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "You:",
                                style: AppStyles.DEFAULT_VERY_SMALL.copyWith(
                                  color: AppColors.red,
                                ),
                              ),
                              Image.asset(
                                AppImages.icDiamond,
                                height: 10,
                                width: 10,
                                fit: BoxFit.fill,
                              ),
                              Text(
                                "${_viewModel.bets[slot].coin}",
                                style: AppStyles.DEFAULT_VERY_SMALL,
                              ),
                            ],
                          ),
                        ),
                    ],
                  ),
                ),
              ),
              Container(
                height: 1,
                color: AppColors.black,
              ),
              Expanded(
                child: Container(
                  width: 68,
                  padding: EdgeInsets.all(1.75),
                  decoration: BoxDecoration(
                    color: AppColors.lightBlue,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(50),
                      bottomRight: Radius.circular(50),
                    ),
                  ),
                  child: Column(
                    children: [
                      Text(
                        "Win ${slot.win} times",
                        style: AppStyles.DEFAULT_VERY_SMALL,
                        textAlign: TextAlign.center,
                      ),
                      if (slot.showCoin > 0)
                        Wrap(
                          spacing: 5,
                          children: List.generate(
                            slot.showCoin,
                            (index) => Image.asset(
                              AppImages.icCoin1,
                              width: 10,
                              height: 10,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
