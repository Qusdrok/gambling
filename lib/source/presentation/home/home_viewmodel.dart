import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../source.dart';

class HomeViewModel extends BaseViewModel {
  List<BetModel> coins = [
    BetModel(
      img: AppImages.icCoin10,
      coin: 10,
    ),
    BetModel(
      img: AppImages.icCoin50,
      coin: 50,
    ),
    BetModel(
      img: AppImages.icCoin100,
      coin: 100,
    ),
    BetModel(
      img: AppImages.icCoin1000,
      coin: 1000,
    ),
  ];

  Map<SlotModel, BetModel> bets = {};
  List<String> resultFoods = [];
  StreamSubscription resultSS;

  int minFoodSelected = 0;
  int foodSelected = -1;
  int coinSelected = -1;
  int profit = 0;
  int coinWin = 0;
  int round = 0;

  bool isResultTime = false;
  bool isUpdateSlot = false;

  init() {}

  initSS() {
    if (resultSS == null) {
      resultSS = new Stream.periodic(Duration(milliseconds: 100)).listen(
        (timer) {
          foodSelected = (foodSelected + 1) % Data.slotModels.length;
          notifyListeners();
        },
      );
    } else {
      resultSS.resume();
    }
  }

  Future<bool> destroySS() async {
    if (bets.containsKey(Data.slotModels[foodSelected])) {
      var c = bets[Data.slotModels[foodSelected]].coin;
      coinWin = c + c * Data.slotModels[foodSelected].win;
      profit = c;

      isUpdateSlot = true;
      AppFirebase.updateSlotWin(
        name: Data.slotModels[foodSelected].name,
        coin: c,
        profit: c,
      );
    }

    resultSS.pause();
    return await _showFinalResult();
  }

  updateCoinType() async {
    setLoading(true);
    await AppFirebase.updateUserCoinType();
    setLoading(false);
    notifyListeners();
  }

  setIsResultTime() {
    isResultTime = !isResultTime;
    notifyListeners();
  }

  setCoinSelected(int index) {
    coinSelected = index;
    notifyListeners();
  }

  String getFoodImage(int index) {
    var i = resultFoods.length - index - 1;
    return resultFoods[i < 0 ? 0 : i];
  }

  setFoodSelected(int index) {
    if (coinSelected != -1) {
      var slot = Data.slotModels[index];
      var bet = BetModel(
        img: slot.img,
        coin: coins[coinSelected].coin,
      );

      if (Data.userModel.currentCoin < bet.coin) return;
      if (!bets.containsKey(slot)) {
        if (minFoodSelected < 6) {
          minFoodSelected++;
          slot.isSelected = true;

          if (slot.showCoin < 3) slot.addCoin();
          bets[slot] = bet;
        }
      } else {
        if (slot.showCoin < 3) slot.addCoin();
        bets[slot] = BetModel(
          img: bet.img,
          coin: bets[slot].coin + bet.coin,
        );
      }

      AppFirebase.updateUserCoin(coin: -bet.coin);
      notifyListeners();
    }
  }

  Future<bool> _showFinalResult() async {
    var index = foodSelected == 8 ? 0 : foodSelected;
    return await showModalBottomSheet(
      isDismissible: false,
      backgroundColor: Colors.black26,
      context: context,
      builder: (c) {
        return WidgetDialogResult(
          index: index,
          coinWin: coinWin,
          profit: profit,
          round: round,
          onEndTimer: () {
            for (var x in Data.slotModels) x.setDisable();
            bets.clear();
            resultFoods.add(Data.slotModels[index].img);

            if (!isUpdateSlot) {
              AppFirebase.updateSlotWin(
                name: Data.slotModels[index].name,
                coin: 0,
              );
            }

            foodSelected = -1;
            coinSelected = -1;
            minFoodSelected = 0;
            isUpdateSlot = false;

            coinWin = 0;
            profit = 0;
            round++;
          },
        );
      },
    );
  }
}

class WidgetDialogResult extends StatefulWidget {
  final int index;
  final int coinWin;
  final int profit;
  final int round;
  final Function onEndTimer;

  const WidgetDialogResult({
    this.index,
    this.coinWin,
    this.profit,
    this.round,
    this.onEndTimer,
  });

  @override
  _WidgetDialogResultState createState() => _WidgetDialogResultState();
}

class _WidgetDialogResultState extends State<WidgetDialogResult>
    with TickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 5),
    );

    _animationController.addStatusListener(
      (status) async {
        switch (status) {
          case AnimationStatus.completed:
            widget.onEndTimer?.call();
            Navigator.pop(context, true);
            break;
        }
      },
    );

    _animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Container(
        height: AppSize.screenHeight / 2,
        child: Stack(
          clipBehavior: Clip.none,
          alignment: Alignment.center,
          children: [
            Positioned(
              top: -35,
              child: Image.asset(
                Data.slotModels[widget.index < 0 ? 0 : widget.index].img,
                fit: BoxFit.fill,
              ),
            ),
            Positioned(
              top: 10,
              right: 10,
              child: AnimatedBuilder(
                animation: _animationController,
                builder: (context, child) {
                  var duration = _animationController.duration *
                      _animationController.value;
                  return Text(
                    "(${5 - (duration.inSeconds % 60)}s)",
                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                      color: Colors.white,
                    ),
                  );
                },
              ),
            ),
            Positioned(
              top: 30,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Text(
                      "BIGO choose ${Data.slotModels[widget.index < 0 ? 0 : widget.index].name}, it has a portion",
                      style: AppStyles.DEFAULT_MEDIUM.copyWith(
                        color: Colors.yellow,
                      ),
                    ),
                    Container(
                      width: AppSize.screenWidth,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          Image.asset(
                            AppImages.icBigoResult,
                            fit: BoxFit.fill,
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    RichText(
                                      text: TextSpan(
                                        text: "The ",
                                        style:
                                            AppStyles.DEFAULT_MEDIUM.copyWith(
                                          color: Colors.white,
                                        ),
                                        children: [
                                          TextSpan(
                                            text: "${widget.round}",
                                            style: AppStyles.DEFAULT_MEDIUM
                                                .copyWith(
                                              color: AppColors.red,
                                            ),
                                          ),
                                          TextSpan(
                                            text: " round's results: ",
                                            style: AppStyles.DEFAULT_MEDIUM
                                                .copyWith(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(width: 5),
                                    Wrap(
                                      spacing: 5,
                                      children: List.generate(
                                        1,
                                        (index) => Image.asset(
                                          Data.slotModels[index].img,
                                          height: 20,
                                          width: 20,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                _buildRowInfo(
                                  title: "This round's winnings:",
                                  img: AppImages.icDiamond,
                                  amount: widget.coinWin,
                                ),
                                _buildRowInfo(
                                  title: "Your selection this round:",
                                  img: AppImages.icCoin1,
                                  amount: widget.profit,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: AppSize.screenWidth,
                      child: Row(
                        children: [
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                height: 1,
                                width: 75,
                                color: Colors.yellow,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              "This round's biggest winner",
                              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                color: Colors.yellow,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                height: 1,
                                width: 75,
                                color: Colors.yellow,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Wrap(
                      spacing: 15,
                      alignment: WrapAlignment.center,
                      children: List.generate(
                        1,
                        (index) => Container(
                          width: 75,
                          height: 115,
                          child: Column(
                            children: [
                              Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  Container(
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                        color: Colors.black,
                                        width: 0.5,
                                      ),
                                    ),
                                    alignment: Alignment.center,
                                    child: Data.userModel != null
                                        ? ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(99),
                                            child: WidgetImageNetwork(
                                              url: Data.userModel.user_avatar,
                                              fit: BoxFit.fill,
                                            ),
                                          )
                                        : Image.asset(
                                            AppImages.icAvatar,
                                            fit: BoxFit.fill,
                                          ),
                                  ),
                                  Positioned(
                                    bottom: 0,
                                    right: -5,
                                    child: Container(
                                      width: 15,
                                      height: 15,
                                      decoration: BoxDecoration(
                                        color: Colors.yellow,
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                          color: Colors.black,
                                          width: 0.5,
                                        ),
                                      ),
                                      alignment: Alignment.center,
                                      child: Text(
                                        "1",
                                        style: AppStyles.DEFAULT_SMALL,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Text(
                                "${Data.userModel.name}",
                                style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                  color: Colors.white,
                                ),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                              SizedBox(height: 5),
                              Expanded(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      AppImages.icDiamond,
                                      height: 12,
                                      width: 12,
                                      fit: BoxFit.fill,
                                    ),
                                    SizedBox(width: 2),
                                    Text(
                                      "${widget.profit}",
                                      style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                        color: Colors.white,
                                      ),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRowInfo({String title, String img, int amount}) {
    return Row(
      children: [
        Text(
          title,
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: Colors.white,
          ),
        ),
        SizedBox(width: 5),
        Image.asset(
          img,
          height: 12,
          width: 12,
          fit: BoxFit.fill,
        ),
        SizedBox(width: 2),
        Text(
          "${amount}",
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: Colors.white,
          ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }
}
