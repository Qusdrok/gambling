import 'package:flutter/material.dart';
import 'package:gambling/source/configs/app_size.dart';
import 'package:gambling/source/configs/app_styles.dart';
import 'package:gambling/source/presentation/login/login_viewmodel.dart';
import 'package:gambling/source/resource/base/base.dart';
import 'package:gambling/source/resource/resource.dart';
import 'package:gambling/source/source.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      viewModel: LoginViewModel(),
      onViewModelReady: (vm) => _viewModel = vm..init(),
      builder: (context, vm, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: _buildBody(),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return GestureDetector(
      onTap: _viewModel.unFocus,
      child: StreamBuilder(
        stream: _viewModel.isLoadingSubject,
        builder: (context, snapshot) {
          bool isLoading = snapshot.data ?? false;
          return _viewModel.isSignIn
              ? _buildSignIn(isLoading)
              : _buildSignUp(isLoading);
        },
      ),
    );
  }

  Widget _buildSignIn(bool isLoading) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          color: Colors.grey[300],
          width: AppSize.screenWidth,
          height: AppSize.screenWidth / 2,
          padding: EdgeInsets.only(bottom: 10),
          alignment: Alignment.center,
          child: WidgetLogo(
            height: AppSize.screenWidth / 3,
            width: AppSize.screenWidth / 3,
          ),
        ),
        SizedBox(height: 15),
        Padding(
          padding: const EdgeInsets.only(left: 20, bottom: 15, right: 20),
          child: Column(
            children: [
              Text(
                "Đăng nhập để trải nghiệm các chức năng hoàn chỉnh",
                style: AppStyles.DEFAULT_LARGE,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 15),
              Form(
                key: _viewModel.formSignIn,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Column(
                  children: [
                    WidgetTextFormField(
                      readOnly: isLoading,
                      inputType: TextInputType.emailAddress,
                      validator: AppValidator.validateEmail(context),
                      width: 25,
                      hintText: "Email",
                      controller: _viewModel.emailController,
                      colorBorder: Colors.grey[300],
                    ),
                    SizedBox(height: 10),
                    WidgetTextFormField(
                      readOnly: isLoading,
                      validator: AppValidator.validatePassword(context),
                      width: 25,
                      hintText: "Mật khẩu",
                      controller: _viewModel.passwordController,
                      colorBorder: Colors.grey[300],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15),
              WidgetButtonGradientAnimation(
                action: () => _viewModel.signIn(),
                isLoading: isLoading,
                title: "Đăng nhập",
                colorTitle: Colors.black,
                width: AppSize.screenWidth,
                colorStart: Colors.grey[300],
                colorEnd: Colors.grey[300],
              ),
              SizedBox(height: 30),
              Row(
                children: [
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        height: 1,
                        width: 75,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Text(
                      "HOẶC",
                      style: AppStyles.DEFAULT_REGULAR,
                    ),
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        height: 1,
                        width: 75,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  WidgetButtonSocial(
                    imageUrl: AppImages.btnFB,
                    onTap: () {},
                    isLoading: isLoading,
                  ),
                  SizedBox(width: 15),
                  WidgetButtonSocial(
                    imageUrl: AppImages.btnGoogle,
                    onTap: () => _viewModel.loginGoogleFirebase(),
                    isLoading: isLoading,
                  ),
                ],
              ),
              SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Bạn chưa có tài khoản ? ",
                    style: AppStyles.DEFAULT_REGULAR.copyWith(
                      color: Colors.grey[400],
                    ),
                  ),
                  GestureDetector(
                    onTap: () => _viewModel.switchScreen(),
                    child: Text(
                      "Đăng ký",
                      style: AppStyles.DEFAULT_REGULAR.copyWith(
                        color: Colors.lightBlueAccent,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildSignUp(bool isLoading) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          color: Colors.grey[300],
          width: AppSize.screenWidth,
          height: AppSize.screenWidth / 2,
          padding: EdgeInsets.only(bottom: 10),
          alignment: Alignment.center,
          child: WidgetLogo(
            height: AppSize.screenWidth / 3,
            width: AppSize.screenWidth / 3,
          ),
        ),
        SizedBox(height: 15),
        Padding(
          padding: const EdgeInsets.only(left: 20, bottom: 15, right: 20),
          child: Column(
            children: [
              Form(
                key: _viewModel.formSignUp,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Column(
                  children: [
                    WidgetTextFormField(
                      readOnly: isLoading,
                      width: 25,
                      validator: AppValidator.validateFullName(context),
                      hintText: "Username",
                      controller: _viewModel.usernameController,
                      colorBorder: Colors.grey[300],
                    ),
                    SizedBox(height: 10),
                    WidgetTextFormField(
                      readOnly: isLoading,
                      inputType: TextInputType.emailAddress,
                      validator: AppValidator.validateEmail(context),
                      width: 25,
                      hintText: "Email",
                      controller: _viewModel.emailController,
                      colorBorder: Colors.grey[300],
                    ),
                    SizedBox(height: 10),
                    WidgetTextFormField(
                      readOnly: isLoading,
                      validator: AppValidator.validatePassword(context),
                      width: 25,
                      hintText: "Mật khẩu",
                      controller: _viewModel.passwordController,
                      colorBorder: Colors.grey[300],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15),
              WidgetButtonGradientAnimation(
                action: () => _viewModel.signUp(),
                isLoading: isLoading,
                title: "Đăng ký",
                colorTitle: Colors.black,
                width: AppSize.screenWidth,
                colorStart: Colors.grey[300],
                colorEnd: Colors.grey[300],
              ),
              SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Bạn đã có tài khoản ? ",
                    style: AppStyles.DEFAULT_REGULAR.copyWith(
                      color: Colors.grey[400],
                    ),
                  ),
                  GestureDetector(
                    onTap: () => _viewModel.switchScreen(),
                    child: Text(
                      "Đăng nhập",
                      style: AppStyles.DEFAULT_REGULAR.copyWith(
                        color: Colors.lightBlueAccent,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
