import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../source.dart';

class LoginViewModel extends BaseViewModel {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController usernameController = TextEditingController();

  GlobalKey<FormState> formSignIn = GlobalKey<FormState>();
  GlobalKey<FormState> formSignUp = GlobalKey<FormState>();

  bool isSignIn = true;

  init() {}

  switchScreen() {
    isSignIn = !isSignIn;
    notifyListeners();
  }

  signUp() {
    unFocus();
    if (!formSignUp.currentState.validate()) return;
    registerWithEmailAndPassword(
      emailOrPhone: emailController.text.trim(),
      password: passwordController.text.trim(),
      username: usernameController.text.trim(),
      onRegisterSuccess: () {
        emailController.clear();
        passwordController.clear();
        usernameController.clear();
        switchScreen();
      },
    );
  }

  signIn() {
    unFocus();
    if (!formSignIn.currentState.validate()) return;
    loginWithEmailAndPassword(
      emailOrPhone: emailController.text.trim(),
      password: passwordController.text.trim(),
    );
  }
}
