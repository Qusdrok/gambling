import 'package:flutter/material.dart';
import 'package:gambling/source/resource/base/base.dart';

import '../../source.dart';

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  NavigationViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      viewModel: NavigationViewModel(),
      onViewModelReady: (vm) => _viewModel = vm..init(),
      builder: (context, vm, child) {
        return Scaffold(
          body: Column(
            children: [
              WidgetAppBar(
                title: _viewModel.title,
                colorTitle: Colors.black,
                colorAppBar: Colors.white,
                titleAlignment: Alignment.centerLeft,
              ),
              Expanded(child: _buildPages()),
              _buildBottomNavBar(),
            ],
          ),
        );
      },
    );
  }

  Widget _buildBottomNavBar() {
    return Container(
      width: AppSize.screenWidth,
      padding: EdgeInsets.only(top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: AppColors.grey,
            blurRadius: 1.2,
          ),
        ],
      ),
      child: Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          Row(
            children: [
              _buildButton(
                index: 0,
                img: AppImages.icFood1,
                title: "Shop",
              ),
              _buildButton(
                index: 1,
                img: AppImages.icFood2,
                title: "Danh sách bạn bè",
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildButton({int index, String img, String title}) {
    return Expanded(
      child: InkWell(
        onTap: () => _viewModel.switchPage(index, title),
        child: Image.asset(
          img,
          width: 25,
          height: 25,
          color:
              _viewModel.currentPage == index ? AppColors.red : AppColors.grey,
        ),
      ),
    );
  }

  Widget _buildPages() {
    switch (_viewModel.currentPage) {
      case 0:
        return ShopScreen();
      case 1:
        return ExchangeScreen();
      default:
        return SizedBox();
    }
  }
}
