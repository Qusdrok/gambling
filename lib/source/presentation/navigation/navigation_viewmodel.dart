import 'package:gambling/source/resource/base/base.dart';

class NavigationViewModel extends BaseViewModel {
  String title = "Shop";
  int currentPage = 0;

  init() {}

  switchPage(int index, String title) {
    currentPage = index;
    this.title = title;
    notifyListeners();
  }
}
