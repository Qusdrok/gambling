import 'package:flutter/material.dart';

import '../../source.dart';

class ShopScreen extends StatefulWidget {
  @override
  _ShopScreenState createState() => _ShopScreenState();
}

class _ShopScreenState extends State<ShopScreen> {
  ShopViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      viewModel: ShopViewModel(),
      onViewModelReady: (vm) => _viewModel = vm..init(),
      builder: (context, vm, child) {
        return Scaffold(
          backgroundColor: Colors.grey,
          body: _buildBody(),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(vertical: 1),
      child: Wrap(
        spacing: 1,
        runSpacing: 1,
        alignment: WrapAlignment.center,
        children: List.generate(
          10,
          (index) => Container(
            width: AppSize.screenWidth / 2 - 1.5,
            color: Colors.white,
            padding: EdgeInsets.all(5),
            child: Column(
              children: [
                Image.asset(
                  AppImages.icFood1,
                  fit: BoxFit.fill,
                ),
                Text(
                  "Airplane",
                  style: AppStyles.DEFAULT_MEDIUM,
                ),
                SizedBox(height: 5),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      AppImages.icDiamond,
                      height: 15,
                      width: 15,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(width: 5),
                    Text(
                      "1313",
                      style: AppStyles.DEFAULT_MEDIUM,
                    ),
                  ],
                ),
                SizedBox(height: 5),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: "Stock: ",
                        style: AppStyles.DEFAULT_MEDIUM.copyWith(
                          color: Colors.grey,
                        ),
                        children: [
                          TextSpan(
                            text: "156/200",
                            style: AppStyles.DEFAULT_MEDIUM.copyWith(
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Container(
                      height: 25,
                      width: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: AppColors.red,
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        "Mua",
                        style: AppStyles.DEFAULT_MEDIUM.copyWith(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
