import 'package:flutter/material.dart';

import '../../source.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SplashViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    AppSize.init(context);
    return BaseWidget(
      viewModel: SplashViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, builder, child) {
        return Scaffold(
          backgroundColor: AppColors.primaryLight,
          body: Stack(
            alignment: Alignment.center,
            children: [
              Image.asset(
                AppImages.imgBg,
                width: AppSize.screenWidth,
                height: AppSize.screenHeight,
                fit: BoxFit.fill,
              ),
              WidgetLogo(
                height: AppSize.screenWidth / 2,
                width: AppSize.screenWidth / 2,
              ),
            ],
          ),
        );
      },
    );
  }
}
