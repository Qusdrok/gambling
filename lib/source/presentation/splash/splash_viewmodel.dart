import 'package:flutter/material.dart';

import '../../source.dart';

class SplashViewModel extends BaseViewModel {
  init() async {
    setLoading(true);
    AppFirebase.updateSlotWin();
    AppFirebase.firebaseAuth.authStateChanges().listen(
      (user) async {
        if (user != null) {
          var doc =
              await AppFirebase.userCollection.doc("userID-${user.uid}").get();
          var data = doc.data();

          if (data != null) {
            var um = UserModel.fromJson(data);
            if (um.isRegister) {
              um.isRegister = false;
              await AppFirebase.uploadUser(um);
              return;
            }

            Data.userModel = um;
            Data.userAvatar =
                Image.network(Data.userModel.user_avatar, fit: BoxFit.cover);

            initPrecacheNetwork();
            await Future.delayed(Duration(milliseconds: 750));
            Navigator.pushNamed(context, Routers.home);
          } else {
            await Future.delayed(Duration(milliseconds: 750));
            Navigator.pushNamed(context, Routers.login);
          }
        } else {
          await Future.delayed(Duration(milliseconds: 750));
          Navigator.pushNamed(context, Routers.login);
        }
      },
    );
    setLoading(false);
  }

  initPrecacheNetwork() {
    precacheImage(Data.userAvatar.image, context);
  }
}
