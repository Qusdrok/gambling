import 'package:dio/dio.dart';

import '../source.dart';

enum LoginType { apple, facebook, google, phone, normal }
enum RequestType { Get, Post }

class AuthenticationRepository {
  AuthenticationRepository._();

  static AuthenticationRepository _instance;

  factory AuthenticationRepository() => _instance ?? AuthenticationRepository._();

  Future<NetworkState<dynamic>> _baseRequestAuthentication({
    RequestType requestType,
    String api,
    dynamic params,
    Function converter,
  }) async {
    bool isDisconnect = await WifiHelper.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    Response response;

    try {
      switch (requestType) {
        case RequestType.Get:
          if (params != null) {
            response = await AppClients().get(api, queryParameters: params);
          } else {
            response = await AppClients().get(api);
          }

          break;
        case RequestType.Post:
          response = await AppClients().post(api, data: params);
          break;
      }

      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: converter,
        ),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
