import 'package:flutter/material.dart';

import '../../source.dart';

class WidgetAppBar extends StatelessWidget {
  final String title;

  final double sizeLeading;
  final double height;

  final bool isBack;

  final Color colorBack;
  final Color colorTitle;
  final Color colorAppBar;

  final Alignment titleAlignment;
  final Widget children;

  final List<Widget> actions;
  final Function actionBack;

  const WidgetAppBar({
    @required this.title,
    this.sizeLeading = 18,
    this.height = 0,
    this.isBack = true,
    this.colorBack = Colors.black,
    this.colorTitle = Colors.white,
    this.colorAppBar,
    this.titleAlignment = Alignment.center,
    this.children,
    this.actions,
    this.actionBack,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: AppSize.screenWidth,
        height: AppValues.HEIGHT_APP_BAR + height,
        color: colorAppBar ?? AppColors.primaryDark,
        child: Column(
          children: [
            Row(
              children: [
                WidgetButtonBack(
                  action: actionBack,
                  size: sizeLeading,
                  color: colorBack,
                  isBack: isBack,
                ),
                Expanded(
                  child: Align(
                    alignment: titleAlignment,
                    child: Text(
                      title,
                      style: AppStyles.DEFAULT_LARGE.copyWith(
                        color: colorTitle,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                actions == null
                    ? Opacity(
                        opacity: 0,
                        child: IconButton(
                          icon: Icon(
                            Icons.arrow_back_ios_rounded,
                            color: colorBack,
                            size: sizeLeading,
                          ),
                          onPressed: () {},
                        ),
                      )
                    : Row(children: actions ?? []),
              ],
            ),
            children ?? Container(),
          ],
        ),
      ),
    );
  }
}

class WidgetButtonBack extends StatelessWidget {
  final Color color;
  final double size;
  final action;
  final bool isBack;

  const WidgetButtonBack({
    this.color = Colors.grey,
    this.size = 18,
    this.action,
    this.isBack = true,
  });

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: isBack ? 1 : 0,
      child: IconButton(
        icon: Icon(
          Icons.arrow_back_ios_rounded,
          color: color,
          size: size,
        ),
        onPressed: isBack ? action ?? () => Navigator.pop(context, true) : null,
      ),
    );
  }
}
