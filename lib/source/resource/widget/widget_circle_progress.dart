import 'package:flutter/material.dart';

import '../../source.dart';

class WidgetCircleProgress extends StatelessWidget {
  final double value;

  final Color valueColor;
  final Color backgroundColor;

  const WidgetCircleProgress({
    this.value,
    this.valueColor,
    this.backgroundColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 25,
      height: 25,
      padding: const EdgeInsets.all(5),
      child: CircularProgressIndicator(
        value: value,
        strokeWidth: 4,
        valueColor: AlwaysStoppedAnimation(valueColor ?? AppColors.primary),
        backgroundColor: backgroundColor ?? AppColors.primaryLight,
      ),
    );
  }
}
