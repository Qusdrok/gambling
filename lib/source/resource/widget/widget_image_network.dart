import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../source.dart';

enum ImageNetworkShape { none, circle }

class WidgetImageNetwork extends StatelessWidget {
  final String url;
  final String assetError;

  final double width;
  final double height;

  final BoxFit fit;
  final ImageNetworkShape shape;

  const WidgetImageNetwork({
    @required this.url,
    this.assetError,
    this.width,
    this.height,
    this.fit,
    this.shape = ImageNetworkShape.none,
  });

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: url,
      width: width,
      height: height,
      placeholder: (_, __) => Center(child: WidgetCircleProgress()),
      imageBuilder: (_, image) {
        switch (shape) {
          case ImageNetworkShape.circle:
            return CircleAvatar(
              radius: ((width ?? height) / 2) ?? 15,
              backgroundImage: image,
            );

          default:
            return Image(
              image: image,
              fit: fit ?? BoxFit.cover,
            );
        }
      },
      errorWidget: (_, __, ___) => CircleAvatar(
        backgroundColor: Colors.white,
        child: Image.asset(
          assetError ?? AppImages.logo,
          fit: BoxFit.fill,
          width: ((width ?? height) * 2 / 3) ?? 15,
        ),
      ),
    );
  }
}
