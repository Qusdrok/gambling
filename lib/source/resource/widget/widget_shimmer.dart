import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class WidgetShimmer extends StatelessWidget {
  final Widget child;

  final Color baseColor;
  final Color highLightColor;

  const WidgetShimmer({
    @required this.child,
    this.baseColor = const Color.fromRGBO(255, 224, 224, 0.88),
    this.highLightColor = Colors.white30,
  });

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      child: child,
      baseColor: baseColor,
      highlightColor: highLightColor,
      enabled: true,
    );
  }
}
