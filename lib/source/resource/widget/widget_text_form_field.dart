import 'package:flutter/material.dart';

import '../../source.dart';

class WidgetTextFormField extends StatefulWidget {
  final String hintText;

  final double height;
  final double width;
  final double circular;

  final bool obscureText;
  final bool readOnly;

  final Widget suffix;
  final Widget prefix;

  final TextEditingController controller;
  final TextInputType inputType;
  final TextStyle hintStyle;
  final Function onTap;
  final Color colorBorder;

  final FormFieldValidator<String> validator;
  final ValueChanged<String> onSubmitted;

  WidgetTextFormField({
    @required this.hintText,
    this.width = 25,
    this.height = 15,
    this.circular = 99,
    this.obscureText = false,
    this.readOnly = false,
    this.suffix,
    this.prefix,
    @required this.controller,
    this.inputType = TextInputType.text,
    this.hintStyle,
    this.onTap,
    this.colorBorder,
    this.validator,
    this.onSubmitted,
  });

  @override
  _WidgetTextFormFieldState createState() => _WidgetTextFormFieldState();
}

class _WidgetTextFormFieldState extends State<WidgetTextFormField> {
  @override
  Widget build(BuildContext context) {
    var _borderTextField = _buildBorderTextField(
      color: widget.colorBorder ?? AppColors.primaryDark,
      circular: widget.circular,
    );

    return TextFormField(
      onTap: widget.onTap ?? () {},
      controller: widget.controller,
      readOnly: widget.readOnly ?? false,
      obscureText: widget.obscureText,
      onFieldSubmitted: widget.onSubmitted,
      validator: widget.validator,
      keyboardType: widget.inputType,
      decoration: InputDecoration(
        suffixIcon: widget.suffix,
        prefixIcon: widget.prefix,
        focusedBorder: _borderTextField,
        enabledBorder: _borderTextField,
        errorBorder: _borderTextField,
        focusedErrorBorder: _borderTextField,
        contentPadding: EdgeInsets.symmetric(
          horizontal: widget.width,
          vertical: widget.height,
        ),
        filled: true,
        fillColor: Colors.white,
        hintText: widget.hintText,
        hintStyle: widget.hintStyle ??
            AppStyles.DEFAULT_REGULAR.copyWith(
              color: AppColors.primaryLight,
            ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField({
    @required Color color,
    @required double circular,
  }) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(circular),
      borderSide: BorderSide(
        color: color,
        width: 0.85,
      ),
    );
  }
}
